﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_01_wf
{
    public partial class Form1 : Form
    {
        double km;
        double mile;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtKM.Text) && string.IsNullOrWhiteSpace(txtMile.Text))
            {
                MessageBox.Show("Please enter a value for either Kilometers or Miles");
            }
            else if (string.IsNullOrWhiteSpace(txtKM.Text))
            {
                mile = Convert.ToInt32(txtMile.Text);
                km = mile * 0.62137119;
                txtKM.Text = km.ToString();
            }
            else if (string.IsNullOrWhiteSpace(txtMile.Text))
            {
                km = Convert.ToInt32(txtKM.Text);
                mile = km / 0.62137119;
                txtMile.Text = mile.ToString();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtMile.Text = "";
            txtKM.Text = "";
        }
    }
}
