﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_03_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            int age;
            string gender;

            age = Convert.ToInt32(txtAge.Text.ToLower());
            gender = txtGender.Text.ToLower();

            txtDisplay.Text = "Hello, you are " + age + " and you are " + gender + ", Congratulations!";
        }
    }
}
