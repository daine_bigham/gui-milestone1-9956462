﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_01_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        double km;
        double mile;
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtKM.Text) && string.IsNullOrWhiteSpace(txtM.Text))
            {
                txtKM.Text = "Please enter a number to convert";
                txtM.Text = "Please enter a number to convert";
            }
            else if (string.IsNullOrWhiteSpace(txtKM.Text))
            {
                mile = Convert.ToInt32(txtM.Text);
                km = mile * 0.62137119;
                txtKM.Text = km.ToString();
            }
            else if (string.IsNullOrWhiteSpace(txtM.Text))
            {
                km = Convert.ToInt32(txtKM.Text);
                mile = km / 0.62137119;
                txtM.Text = mile.ToString();
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtM.Text = "";
            txtKM.Text = "";
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Exit();
        }
    }
}
