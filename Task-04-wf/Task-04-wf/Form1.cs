﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_04_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string input = txtEnter.Text.ToLower();

            Dictionary<int, string> dict = new Dictionary<int, string>();
            dict.Add(1, "banana");
            dict.Add(2, "apple");
            dict.Add(3, "peach");
            dict.Add(4, "plum");
            dict.Add(5, "nectarine");
            dict.Add(6, "grapes");
            dict.Add(7, "pear");

            if (dict.ContainsValue(input))
            {
                txtDisplay.Text = "That item is already in the list";
            }
            else
                txtDisplay.Text = "I thought I'd written that down";
        }
    }
}
