﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03_console
{
    class Program
    {
        
        static void Main(string[] args)
        {
            string input;
            int choice1;
            int choice2;
            string out1 = "";
            string out2 = "";

            Console.WriteLine("Please select an option: \n1. You are Male\n2. You are Female");
            input = Console.ReadLine();
            int.TryParse(input, out choice1);

                switch (choice1)
                {
                    case 1:
                        Console.Clear();
                        out1 = "male";
                        Console.WriteLine("So you are a {0}? are you:\n1. Under the age of 20\n2. Over the age of 20\n3. Over the age of 50", out1);
                        input = Console.ReadLine();
                        int.TryParse(input, out choice2);
                        do
                        {
                            if (choice2 == 1)
                            {
                                out2 = "Under 20";
                                break;
                            }
                            else if (choice2 == 2)
                            {
                                out2 = "over 20";
                                break;
                            }
                            else if (choice2 == 3)
                            {
                                out2 = "over 50";
                                break;
                            }
                            else
                                Console.WriteLine("Please enter either 1, 2 or 3");
                        } while (choice2 != 1 || choice2 != 2 || choice2 != 3);
                        break;

                    case 2:
                        Console.Clear();
                        out1 = "female";
                        Console.WriteLine("So you are a {0}? are you:\n1. Under the age of 20\n2. Over the age of 20\n3. Over the age of 50", out1);
                        input = Console.ReadLine();
                        int.TryParse(input, out choice2);

                        do
                        {
                            if (choice2 == 1)
                            {
                                out2 = "Under 20";
                                break;
                            }
                            else if (choice2 == 2)
                            {
                                out2 = "over 20";
                                break;
                            }
                            else if (choice2 == 3)
                            {
                                out2 = "over 50";
                                break;
                            }
                            else
                                Console.WriteLine("Please enter either 1, 2 or 3");
                        } while (choice2 != 1 || choice2 != 2 || choice2 != 3);
                        break;

                    default:
                        Console.WriteLine("Please select either 1 or 2");
                        break;
                }
            

            Console.WriteLine("So you are a {0} who is {1} years of age, Congratulations!", out1, out2);
            Console.Read();
        }
    }
}
