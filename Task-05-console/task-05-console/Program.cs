﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_05_console
{
    class Program
    {
        static void Main(string[] args)
        {
            string input;
            int player;
            int computer;
            int score = 0;
            int i;

            for (i = 0; i < 5; i++)
            {
                

                Console.WriteLine("Please choose a number between 1-5 to try and beat the machine!");
                input = Console.ReadLine();
                int.TryParse(input, out player);

                computer = R();

                if (player == computer)
                {
                    score++;
                    Console.WriteLine("Your number is {0} and the computer playes {1} you scored!", player, computer);
                    
                }
                else if(player != computer)
                {
                    Console.WriteLine("Bad guess friend");
                }
            }

            Console.WriteLine("You scored {0} points against the computer", score);
            Console.Read();
        }

        static Random rand = new Random();
        static int R()
        {
            int n = rand.Next(6);

            return n;
        }
    }
}
