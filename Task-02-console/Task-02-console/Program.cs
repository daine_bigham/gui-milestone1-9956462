﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02_console
{
    class Program
    {
        static void Main(string[] args)
        {
            List<double> numbers = new List<double>();
            string input;
            double total;
            double number;

            Console.Write("Welcome to Shopping Helper Plus\nPlease enter your order then press ENTER to exit\n: ");
            input = Console.ReadLine();
            double.TryParse(input, out number);
            numbers.Add(number);

            while (input != "")
            {
                Console.Write(": ");
                input = Console.ReadLine();
                double.TryParse(input, out number);
                numbers.Add(number);
                
            }

            if (input == "")
            {
                total = numbers.Sum();
                total = Math.Round(total * 1.15, 2);
                Console.WriteLine("Your total cost plus GST is: {0}", total);
                Console.Read();
            }
        }
    }
}
