﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01_console
{
    class Program
    {
        static void Main(string[] args)
        {
            //name local variable to be used in program
            double km;
            double mile;
            double result;
            string line;
            
            //initiate while loop that will continue indefinitely
            while (true)
            {
                //create console output and read user input
                Console.Clear();
                Console.WriteLine("Welcome to Distance Converter 2.3");
                Console.WriteLine("1) for Miles -> Kilometres\n2) for Kilometres -> Miles");
                line = Console.ReadLine();

                //begin if statement to decide whether to convert mile to km, km to miles or throw error
                if (line == "1")
                {
                        Console.Clear();
                        Console.Write("Enter distance to be converted: ");
                        //convert input from type string to type int, calculate the result and round the result to 2 decimal places
                        km = Convert.ToInt32(Console.ReadLine());
                        result = Math.Round(km * 0.62137119, 2);
                        Console.WriteLine("{0} kilometres is {1} miles", km, result);
                        Console.Write("Press ENTER to continue or ESC to exit");

                        //enable exit by ESC key to close program
                        var key = Console.ReadKey();

                        if (key.Key == ConsoleKey.Escape)
                        {
                            Environment.Exit(0);
                        }
                }
                else if (line == "2")
                {
                    Console.Clear();
                    Console.Write("Enter distance to be converted: ");
                    mile = Convert.ToInt32(Console.ReadLine());
                    result = Math.Round(mile / 0.62137119, 2);
                    Console.WriteLine("{0} miles is {1} kilometres", mile, result);
                    Console.Write("Press ENTER to continue or ESC to exit");

                    var key = Console.ReadKey();

                    if (key.Key == ConsoleKey.Escape)
                    {
                        Environment.Exit(0);
                    }
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Error! Please Enter 1 or 2");
                    Console.ReadLine();
                }
            }
        }
    }
}
