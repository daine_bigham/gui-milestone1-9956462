﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04_console
{
    class Program
    {
        static void Main(string[] args)
        {
            bool close = false;

            Dictionary<int, string> dict = new Dictionary<int, string>();
                dict.Add(1, "banana");
                dict.Add(2, "apple");
                dict.Add(3, "peach");
                dict.Add(4, "plum");
                dict.Add(5, "nectarine");
                dict.Add(6, "grapes");
                dict.Add(7, "pear");

            do
            {
                Console.Clear();
                Console.WriteLine("Please enter a fruit to add to the shopping list");
                string input = Console.ReadLine().ToLower();

                if (dict.ContainsValue(input))
                {
                    Console.WriteLine("That item is already in the list");
                    Console.Read();
                    Console.Clear();
                }
                else
                    close = true;
                    Console.WriteLine("I thought I'd written that down...");
                    Console.Read();
            } while (close == false);
        }
    }
}
