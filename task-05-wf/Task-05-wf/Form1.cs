﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_05_wf
{
    public partial class Form1 : Form
    {
        string input;
        int player;
        int computer;
        int score = 0;
        int i = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            computer = R();
            player = Convert.ToInt32(txtPlayer.Text);
            i++;

                if (player == computer)
                {
                    score++;
                }

                if (i < 5)
                {
                    txtScore.Text = txtScore.Text + "\r\nPC: " + computer + "\r\nPlayer: " + player + "\r\nScore: " + score;
                }
                else if (i > 5)
                {
                    txtScore.Text = "Game Over!";
                    i = 0;
                }
        }

        static Random rand = new Random();
        static int R()
        {
            int n = rand.Next(6);

            return n;
        }
    }
}
