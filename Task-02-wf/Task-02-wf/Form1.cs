﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_02_wf
{
    public partial class Form1 : Form
    {
        List<double> numbers = new List<double>();
        string input;
        double total;
        double number;

        public Form1()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            number = Convert.ToInt32(txtItem.Text);
            numbers.Add(number);
        }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            total = Math.Round(numbers.Sum() * 1.15, 2);
            txtTotal.Text = Convert.ToString(total); 

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
